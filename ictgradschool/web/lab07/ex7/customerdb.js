window.onload = function(){
var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];
console.log (customers[0].gender);
    var table =document.getElementById("contents");
    console.log(table);
    for(let i in customers){
        var row =table.insertRow(i);
        var navigator1 = row.insertCell(0);
        var navigator2 = row.insertCell(1);
        var navigator3 = row.insertCell(2);
        var navigator4 = row.insertCell(3);
        var navigator5 = row.insertCell(4);
        navigator1.innerHTML = customers[i].name;
        navigator2.innerHTML = customers[i].gender;
        navigator3.innerHTML = customers[i].year_born;
        navigator4.innerHTML = customers[i].joined;
        navigator5.innerHTML = customers[i].num_hires;
    }
 var malesAndFemales=document.getElementById("malesAndFemales");
 malesAndFemales.innerHTML = "Total number of Males: "+males()+"<br> Total number of Females: "+females();
 var ages = document.getElementById("ages");
 ages.innerHTML = "Total number of people below 30: "+ ageBelow30()+"<br> Total number of people aged between 31-64: "+ageBetween()+"<br> Total number of people aged above 65: "+ageAbove();
 var loyalty = document.getElementById("loyalty");
 loyalty.innerHTML = "Total number of gold loyalty members: "+loyatlyGold()+"<br> Total number of silver members: "+loyatlySilver()+"<br>Total number of bronze members: "+loyatlyBronze();

 function males(){
 	let maleCount = 0;
 	for(let i =0;i<customers.length;i++){
 		if(customers[i].gender === "male"){
 			maleCount++;
 		 }
 	}return maleCount;
 }
 function females(){
 	let femaleCount = 0;
 	for(let i =0;i<customers.length;i++){
 		if(customers[i].gender === "female"){
 			femaleCount++;
 		 }
 	}return femaleCount;
 }
 function ageBelow30(){
 	let belowCount = 0;
 		for(let i =0;i<customers.length;i++){
 			if(customers[i].year_born>=1988){
 				belowCount++
 			}
 }return belowCount;
}
function ageBetween(){
 	let betweenCount = 0;
 		for(let i =0;i<customers.length;i++){
 			if((customers[i].year_born>=1954)&& (customers[i].year_born<1988)){
 				betweenCount++
 			}
 }return betweenCount;
}
 function ageAbove(){
 	let aboveCount = 0;
 		for(let i =0;i<customers.length;i++){
 			if(customers[i].year_born<1954){
 				aboveCount++
 			}
 }return aboveCount;
}
function loyatlyGold(){
	let goldCount = 0;
	for(let i =0;i<customers.length;i++){
 			if((customers[i].num_hires)/((2018-customers[i].joined)*52)>4){
 				goldCount++
 			}

}return goldCount;
}
function loyatlySilver(){
	let silverCount = 0;
	for(let i =0;i<customers.length;i++){
		let weeklyHires = customers[i].num_hires/(((2018-customers[i].joined))*52);
 			if((weeklyHires<=4) && (weeklyHires>=1)){
 				silverCount++
 			}

}return silverCount;
}
function loyatlyBronze(){
	let bronzeCount = 0;
	for(let i =0;i<customers.length;i++){
 			if((customers[i].num_hires)/((2018-customers[i].joined)*52)<1){
 				bronzeCount++
 			}

}return bronzeCount;
}
}
	//var tblRow = "<tr>"+"<td>"+customers[i].name+"</td>"+"<td>"+customers[i].gender+"</td>"+"<td>"+customers[i].year_born+"</td>"+"<td>"+customers[i].joined+"</td>"+"<td>"+customers[i].num_hires+"</td>" +"</tr>";